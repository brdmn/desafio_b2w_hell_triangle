#!/usr/bin/python
# -*- coding: utf-8 -*-

from lib.hell_triangle import HellTriangle
from unittest import TestCase, main


class Triangle_Test(TestCase):

    def setUp(self):

        valid_triangle = [[6], [3, 5], [9, 7, 1], [4, 6, 8, 4]]
        invalid_triangle1 = [[6], [3, 5], [9, 7, 1, 20], [4, 6, 8, 4, 30]]
        invalid_triangle2 = ["a", [1, 2, 3]]
        invalid_triangle3 = None
        invalid_triangle4 = [[1], [1, 2], ["1", 2, 3]]

        self.triangle = HellTriangle(valid_triangle)
        self.triangle_1 = HellTriangle(invalid_triangle1)
        self.triangle_2 = HellTriangle(invalid_triangle2)
        self.triangle_3 = HellTriangle(invalid_triangle3)
        self.triangle_4 = HellTriangle(invalid_triangle4)

    def test_triangle(self):

        self.assertTrue(self.triangle.valid_triangle())

        self.assertFalse(self.triangle_1.valid_triangle())
        self.assertFalse(self.triangle_2.valid_triangle())
        self.assertFalse(self.triangle_3.valid_triangle())
        self.assertFalse(self.triangle_4.valid_triangle())

    def test_maximum_total(self):

        self.assertEqual(self.triangle.maximum_total(), 26)


if __name__ == '__main__':
    main()
