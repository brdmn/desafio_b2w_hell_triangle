#!/usr/bin/python
# -*- coding: utf-8 -*-

from lib.hell_triangle import HellTriangle


def main():

    triangle = [[6], [3, 5], [9, 7, 1], [4, 6, 8, 4]]

    tri_obj = HellTriangle(triangle)
    print('Maximum total = {}'.format(tri_obj.maximum_total()))


if __name__ == '__main__':
    main()
