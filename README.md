# B2W Challenge - Hell Triangle

### Prerequisites :

    1. Unix Shell
    2. Python >= 2.7 


### Running:

    You can run this code doing:
        1. cd desafio_b2w_hell_triangle
        2. python main.py

### Tests:
    
    You can run tests doing:
        1. cd desafio_b2w_hell_triangle
        2. python -m unittest discover