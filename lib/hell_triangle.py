#!/usr/bin/python
# -*- coding: utf-8 -*-


class HellTriangle:

    '''
    This class expect receive a triangle represented as multidimensional array
    of numbers.

        Example :
            triangle = [[6],[3,5],[9,7,1],[4,6,8,4]]
    '''

    def __init__(self, triangle):

        self.triangle = triangle

    def maximum_total(self):
        '''
        Given a triangle of numbers, find the maximum total from top to bottom.

        Example :

            triangle = [[6],[3,5],[9,7,1],[4,6,8,4]]

        In this triangle the maximum total is: 6 + 5 + 7 + 8 = 26

        An element can only be summed with one of the two nearest elements
        in the next row.

        For example: The element 3 in the 2nd row can only be summed
        with 9 and 7, but not with 1
        '''

        if not self.valid_triangle():
            raise ValueError('Invalid triangle format.')

        # Triangle rows
        rows_total = len(self.triangle)

        for row in range(rows_total - 2, -1, -1):

            left, right = 0, 1

            for value, _ in enumerate(self.triangle[row]):

                max_value = max(self.triangle[row + 1][left],
                                self.triangle[row + 1][right])

                self.triangle[row][value] += max_value

            left = right, right + 1

        return self.triangle[0][0]

    def valid_triangle(self):
        '''
        Check if the triangle given is valid.
        A valid triangle is a multidimensional array of numbers.
        '''

        if type(self.triangle) is not list:
            return False

        counter = 0
        for row in self.triangle:

            if type(row) is not list:
                return False

            if not all(type(item) is int or type(item) is float or
                       type(item) is long for item in row):
                return False

            if len(row) == counter + 1:
                counter += 1
            else:
                return False

        return True
